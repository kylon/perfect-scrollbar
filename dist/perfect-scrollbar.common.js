/*!
 * perfect-scrollbar v1.4.x
 * (c) 2018 Hyunje Jun
 * @license MIT
 *
 * + ataklychev [c91f263e6b3556]
 * + MichaelTamm [PR #841]
 * + zalesky [PR #829]
 * + darron1217 [PR #798]
 * + goldfire [b74bd2d54a39d83]
 * + kylon [Add ability to freeze/unfreeze the scrollbar]
 * + kylon [Add resetPosition function]
 */
'use strict';

function get(element) {
    return getComputedStyle(element);
}

function set(element, obj) {
    for (const key in obj) {
        let val = obj[key];

        if (typeof val === 'number')
            val = val + "px";

        element.style[key] = val;
    }

    return element;
}

function div(className) {
    const div = document.createElement('div');

    div.className = className;

    return div;
}

const elMatches = typeof Element !== 'undefined' &&
    (Element.prototype.matches || Element.prototype.webkitMatchesSelector ||
        Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector);

function matches(element, query) {
    if (!elMatches)
        throw new Error('No element matching method supported');

    return elMatches.call(element, query);
}

function remove(element) {
    if (element.remove)
        element.remove();
    else if (element.parentNode)
        element.parentNode.removeChild(element);
}

function queryChildren(element, selector) {
    return Array.prototype.filter.call(element.children, function (child) {
        return matches(child, selector);
    });
}

let cls = {
    main: 'ps',
    element: {
        thumb: function (x) { return ("ps__thumb-" + x); },
        rail: function (x) { return ("ps__rail-" + x); },
        consuming: 'ps__child--consume',
    },
    state: {
        focus: 'ps--focus',
        clicking: 'ps--clicking',
        active: function (x) { return ("ps--active-" + x); },
        scrolling: function (x) { return ("ps--scrolling-" + x); },
    },
};

/*
 * Helper methods
 */
let scrollingClassTimeout = { x: null, y: null };

function addScrollingClass(i, x) {
    const className = cls.state.scrolling(x);
    let classList = i.element.classList;

    if (classList.contains(className)) {
        clearTimeout(scrollingClassTimeout[x]);
    } else {
        classList.add(className);
    }
}

function removeScrollingClass(i, x) {
    scrollingClassTimeout[x] = setTimeout(
        function () { return i.isAlive && i.element.classList.remove(cls.state.scrolling(x)); },
        i.settings.scrollingThreshold
    );
}

function setScrollingClassInstantly(i, x) {
    addScrollingClass(i, x);
    removeScrollingClass(i, x);
}

const EventElement = function EventElement(element) {
    this.element = element;
    this.handlers = {};
};

let prototypeAccessors = { isEmpty: { configurable: true } };

EventElement.prototype.bind = function bind(eventName, handler) {
    if (typeof this.handlers[eventName] === 'undefined') {
        this.handlers[eventName] = [];
    }
    this.handlers[eventName].push(handler);
    this.element.addEventListener(eventName, handler, false);
};

EventElement.prototype.unbind = function unbind(eventName, target) {
    let this$1 = this;

    this.handlers[eventName] = this.handlers[eventName].filter(function (handler) {
        if (target && handler !== target) {
            return true;
        }
        this$1.element.removeEventListener(eventName, handler, false);
        return false;
    });
};

EventElement.prototype.unbindAll = function unbindAll() {
    let this$1 = this;

    for (let name in this$1.handlers) {
        this$1.unbind(name);
    }
};

prototypeAccessors.isEmpty.get = function () {
    let this$1 = this;

    return Object.keys(this.handlers).every(
        function (key) { return this$1.handlers[key].length === 0; }
    );
};

Object.defineProperties(EventElement.prototype, prototypeAccessors);

let EventManager = function EventManager() {
    this.eventElements = [];
};

EventManager.prototype.eventElement = function eventElement(element) {
    let ee = this.eventElements.filter(function (ee) { return ee.element === element; })[0];
    if (!ee) {
        ee = new EventElement(element);
        this.eventElements.push(ee);
    }
    return ee;
};

EventManager.prototype.bind = function bind(element, eventName, handler) {
    this.eventElement(element).bind(eventName, handler);
};

EventManager.prototype.unbind = function unbind(element, eventName, handler) {
    let ee = this.eventElement(element);
    ee.unbind(eventName, handler);

    if (ee.isEmpty) {
        // remove
        this.eventElements.splice(this.eventElements.indexOf(ee), 1);
    }
};

EventManager.prototype.unbindAll = function unbindAll() {
    this.eventElements.forEach(function (e) { return e.unbindAll(); });
    this.eventElements = [];
};

EventManager.prototype.once = function once(element, eventName, handler) {
    let ee = this.eventElement(element);
    let onceHandler = function (evt) {
        ee.unbind(eventName, onceHandler);
        handler(evt);
    };
    ee.bind(eventName, onceHandler);
};

function createEvent(name) {
    if (typeof window.CustomEvent === 'function') {
        return new CustomEvent(name);
    } else {
        let evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(name, false, false, undefined);
        return evt;
    }
}

let processScrollDiff = function (i, axis, diff, useScrollingClass, forceFireReachEvent) {
    if (useScrollingClass === void 0) useScrollingClass = true;
    if (forceFireReachEvent === void 0) forceFireReachEvent = false;

    let fields;
    if (axis === 'top') {
        fields = [
            'contentHeight',
            'containerHeight',
            'scrollTop',
            'y',
            'up',
            'down'];
    } else if (axis === 'left') {
        fields = [
            'contentWidth',
            'containerWidth',
            'scrollLeft',
            'x',
            'left',
            'right'];
    } else {
        throw new Error('A proper axis should be provided');
    }

    processScrollDiff$1(i, diff, fields, useScrollingClass, forceFireReachEvent);
};

function processScrollDiff$1(
    i,
    diff,
    ref,
    useScrollingClass,
    forceFireReachEvent
) {
    let contentHeight = ref[0];
    let containerHeight = ref[1];
    let scrollTop = ref[2];
    let y = ref[3];
    let up = ref[4];
    let down = ref[5];
    if (useScrollingClass === void 0) useScrollingClass = true;
    if (forceFireReachEvent === void 0) forceFireReachEvent = false;

    let element = i.element;

    // reset reach
    i.reach[y] = null;

    // 1 for subpixel rounding
    if (element[scrollTop] < 1) {
        i.reach[y] = 'start';
    }

    // 1 for subpixel rounding
    if (element[scrollTop] > i[contentHeight] - i[containerHeight] - 1) {
        i.reach[y] = 'end';
    }

    if (diff) {
        element.dispatchEvent(createEvent(("ps-scroll-" + y)));

        if (diff < 0) {
            element.dispatchEvent(createEvent(("ps-scroll-" + up)));
        } else if (diff > 0) {
            element.dispatchEvent(createEvent(("ps-scroll-" + down)));
        }

        if (useScrollingClass) {
            setScrollingClassInstantly(i, y);
        }
    }

    if (i.reach[y] && (diff || forceFireReachEvent)) {
        element.dispatchEvent(createEvent(("ps-" + y + "-reach-" + (i.reach[y]))));
    }
}

function toInt(x) {
    return parseInt(x, 10) || 0;
}

function isEditable(el) {
    return (
        matches(el, 'input,[contenteditable]') ||
        matches(el, 'select,[contenteditable]') ||
        matches(el, 'textarea,[contenteditable]') ||
        matches(el, 'button,[contenteditable]')
    );
}

function outerWidth(element) {
    let styles = get(element);
    return (
        toInt(styles.width) +
        toInt(styles.paddingLeft) +
        toInt(styles.paddingRight) +
        toInt(styles.borderLeftWidth) +
        toInt(styles.borderRightWidth)
    );
}

let env = {
    isWebKit:
        typeof document !== 'undefined' &&
        'WebkitAppearance' in document.documentElement.style,
    supportsTouch:
        typeof window !== 'undefined' &&
        ('ontouchstart' in window ||
            ('maxTouchPoints' in window.navigator && window.navigator.maxTouchPoints > 0) ||
            (window.DocumentTouch && document instanceof window.DocumentTouch)),
    supportsIePointer:
        typeof navigator !== 'undefined' && navigator.msMaxTouchPoints,
    isChrome:
        typeof navigator !== 'undefined' &&
        /Chrome/i.test(navigator && navigator.userAgent),
};

let updateGeometry = function (i) {
    let element = i.element;
    let roundedScrollTop = Math.floor(element.scrollTop);

    i.containerWidth = element.clientWidth;
    i.containerHeight = element.clientHeight;
    i.contentWidth = element.scrollWidth;
    i.contentHeight = element.scrollHeight;

    if (!element.contains(i.scrollbarXRail)) {
        // clean up and append
        queryChildren(element, cls.element.rail('x')).forEach(function (el) { return remove(el); }
        );
        element.appendChild(i.scrollbarXRail);
    }
    if (!element.contains(i.scrollbarYRail)) {
        // clean up and append
        queryChildren(element, cls.element.rail('y')).forEach(function (el) { return remove(el); }
        );
        element.appendChild(i.scrollbarYRail);
    }

    if (
        !i.settings.suppressScrollX &&
        i.containerWidth + i.settings.scrollXMarginOffset < i.contentWidth
    ) {
        i.scrollbarXActive = true;
        i.railXWidth = i.containerWidth - i.railXMarginWidth;
        i.railXRatio = i.containerWidth / i.railXWidth;
        i.scrollbarXWidth = getThumbSize(
            i,
            toInt(i.railXWidth * i.containerWidth / i.contentWidth)
        );
        i.scrollbarXLeft = toInt(
            (i.negativeScrollAdjustment + element.scrollLeft) *
            (i.railXWidth - i.scrollbarXWidth) /
            (i.contentWidth - i.containerWidth)
        );
    } else {
        i.scrollbarXActive = false;
    }

    if (
        !i.settings.suppressScrollY &&
        i.containerHeight + i.settings.scrollYMarginOffset < i.contentHeight
    ) {
        i.scrollbarYActive = true;
        i.railYHeight = i.containerHeight - i.railYMarginHeight;
        i.railYRatio = i.containerHeight / i.railYHeight;
        i.scrollbarYHeight = getThumbSize(
            i,
            toInt(i.railYHeight * i.containerHeight / i.contentHeight)
        );
        i.scrollbarYTop = toInt(
            roundedScrollTop *
            (i.railYHeight - i.scrollbarYHeight) /
            (i.contentHeight - i.containerHeight)
        );
    } else {
        i.scrollbarYActive = false;
    }

    if (i.scrollbarXLeft >= i.railXWidth - i.scrollbarXWidth) {
        i.scrollbarXLeft = i.railXWidth - i.scrollbarXWidth;
    }
    if (i.scrollbarYTop >= i.railYHeight - i.scrollbarYHeight) {
        i.scrollbarYTop = i.railYHeight - i.scrollbarYHeight;
    }

    updateCss(element, i);

    if (i.scrollbarXActive) {
        element.classList.add(cls.state.active('x'));
    } else {
        element.classList.remove(cls.state.active('x'));
        i.scrollbarXWidth = 0;
        i.scrollbarXLeft = 0;
        element.scrollLeft = 0;
    }
    if (i.scrollbarYActive) {
        element.classList.add(cls.state.active('y'));
    } else {
        element.classList.remove(cls.state.active('y'));
        i.scrollbarYHeight = 0;
        i.scrollbarYTop = 0;
        element.scrollTop = 0;
    }
};

function getThumbSize(i, thumbSize) {
    if (i.settings.minScrollbarLength) {
        thumbSize = Math.max(thumbSize, i.settings.minScrollbarLength);
    }
    if (i.settings.maxScrollbarLength) {
        thumbSize = Math.min(thumbSize, i.settings.maxScrollbarLength);
    }
    return thumbSize;
}

function updateCss(element, i) {
    let xRailOffset = { width: i.railXWidth };
    let roundedScrollTop = Math.floor(element.scrollTop);

    if (i.isRtl) {
        xRailOffset.left =
            i.negativeScrollAdjustment +
            element.scrollLeft +
            i.containerWidth -
            i.contentWidth;
    } else {
        xRailOffset.left = element.scrollLeft;
    }
    if (i.isScrollbarXUsingBottom) {
        xRailOffset.bottom = i.scrollbarXBottom - roundedScrollTop;
    } else {
        xRailOffset.top = i.scrollbarXTop + roundedScrollTop;
    }
    set(i.scrollbarXRail, xRailOffset);

    let yRailOffset = { top: roundedScrollTop, height: i.railYHeight };
    if (i.isScrollbarYUsingRight) {
        if (i.isRtl) {
            yRailOffset.right =
                i.contentWidth -
                (i.negativeScrollAdjustment + element.scrollLeft) -
                i.scrollbarYRight -
                i.scrollbarYOuterWidth;
        } else {
            yRailOffset.right = i.scrollbarYRight - element.scrollLeft;
        }
    } else {
        if (i.isRtl) {
            yRailOffset.left =
                i.negativeScrollAdjustment +
                element.scrollLeft +
                i.containerWidth * 2 -
                i.contentWidth -
                i.scrollbarYLeft -
                i.scrollbarYOuterWidth;
        } else {
            yRailOffset.left = i.scrollbarYLeft + element.scrollLeft;
        }
    }
    set(i.scrollbarYRail, yRailOffset);

    set(i.scrollbarX, {
        left: i.scrollbarXLeft,
        width: i.scrollbarXWidth - i.railBorderXWidth,
    });
    set(i.scrollbarY, {
        top: i.scrollbarYTop,
        height: i.scrollbarYHeight - i.railBorderYWidth,
    });
}

let clickRail = function (i) {
    i.event.bind(i.scrollbarY, 'mousedown', function (e) { return e.stopPropagation(); });
    i.event.bind(i.scrollbarYRail, 'mousedown', function (e) {
        let positionTop =
            e.pageY -
            window.pageYOffset -
            i.scrollbarYRail.getBoundingClientRect().top;
        let direction = positionTop > i.scrollbarYTop ? 1 : -1;

        i.element.scrollTop += direction * i.containerHeight;
        updateGeometry(i);

        e.stopPropagation();
    });

    i.event.bind(i.scrollbarX, 'mousedown', function (e) { return e.stopPropagation(); });
    i.event.bind(i.scrollbarXRail, 'mousedown', function (e) {
        let positionLeft =
            e.pageX -
            window.pageXOffset -
            i.scrollbarXRail.getBoundingClientRect().left;
        let direction = positionLeft > i.scrollbarXLeft ? 1 : -1;

        i.element.scrollLeft += direction * i.containerWidth;
        updateGeometry(i);

        e.stopPropagation();
    });
};

let dragThumb = function (i) {
    bindMouseScrollHandler(i, [
        'containerWidth',
        'contentWidth',
        'pageX',
        'railXWidth',
        'scrollbarX',
        'scrollbarXWidth',
        'scrollLeft',
        'x',
        'scrollbarXRail']);
    bindMouseScrollHandler(i, [
        'containerHeight',
        'contentHeight',
        'pageY',
        'railYHeight',
        'scrollbarY',
        'scrollbarYHeight',
        'scrollTop',
        'y',
        'scrollbarYRail']);
};

function bindMouseScrollHandler(
    i,
    ref
) {
    let containerHeight = ref[0];
    let contentHeight = ref[1];
    let pageY = ref[2];
    let railYHeight = ref[3];
    let scrollbarY = ref[4];
    let scrollbarYHeight = ref[5];
    let scrollTop = ref[6];
    let y = ref[7];
    let scrollbarYRail = ref[8];

    let element = i.element;

    let startingScrollTop = null;
    let startingMousePageY = null;
    let scrollBy = null;

    function mouseMoveHandler(e) {
        if (i.isFrozen)
            return;

        element[scrollTop] =
            startingScrollTop + scrollBy * (e[pageY] - startingMousePageY);
        addScrollingClass(i, y);
        updateGeometry(i);

        e.stopPropagation();
        e.preventDefault();
    }

    function mouseUpHandler() {
        removeScrollingClass(i, y);
        i[scrollbarYRail].classList.remove(cls.state.clicking);
        i.event.unbind(i.ownerDocument, 'mousemove', mouseMoveHandler);
    }

    i.event.bind(i[scrollbarY], 'mousedown', function (e) {
        startingScrollTop = element[scrollTop];
        startingMousePageY = e[pageY];
        scrollBy =
            (i[contentHeight] - i[containerHeight]) /
            (i[railYHeight] - i[scrollbarYHeight]);

        i.event.bind(i.ownerDocument, 'mousemove', mouseMoveHandler);
        i.event.once(i.ownerDocument, 'mouseup', mouseUpHandler);

        i[scrollbarYRail].classList.add(cls.state.clicking);

        e.stopPropagation();
        e.preventDefault();
    });
}

let keyboard = function (i) {
    let element = i.element;

    let elementHovered = function () { return matches(element, ':hover'); };
    let scrollbarFocused = function () { return matches(i.scrollbarX, ':focus') || matches(i.scrollbarY, ':focus'); };

    function shouldPreventDefault(deltaX, deltaY) {
        let scrollTop = Math.floor(element.scrollTop);
        if (deltaX === 0) {
            if (!i.scrollbarYActive) {
                return false;
            }
            if (
                (scrollTop === 0 && deltaY > 0) ||
                (scrollTop >= i.contentHeight - i.containerHeight && deltaY < 0)
            ) {
                return !i.settings.wheelPropagation;
            }
        }

        let scrollLeft = element.scrollLeft;
        if (deltaY === 0) {
            if (!i.scrollbarXActive) {
                return false;
            }
            if (
                (scrollLeft === 0 && deltaX < 0) ||
                (scrollLeft >= i.contentWidth - i.containerWidth && deltaX > 0)
            ) {
                return !i.settings.wheelPropagation;
            }
        }
        return true;
    }

    i.event.bind(i.ownerDocument, 'keydown', function (e) {
        if (i.isFrozen || (e.isDefaultPrevented && e.isDefaultPrevented()) || e.defaultPrevented ||
                (!elementHovered() && !scrollbarFocused()))
            return;

        let activeElement = document.activeElement ? document.activeElement:i.ownerDocument.activeElement;

        if (activeElement) {
            if (activeElement.tagName === 'IFRAME') {
                activeElement = activeElement.contentDocument.activeElement;
            } else {
                // go deeper if element is a webcomponent
                while (activeElement.shadowRoot) {
                    activeElement = activeElement.shadowRoot.activeElement;
                }
            }

            if (isEditable(activeElement))
                return;
        }

        let deltaX = 0;
        let deltaY = 0;

        switch (e.which) {
            case 37: // left
                if (e.metaKey) {
                    deltaX = -i.contentWidth;
                } else if (e.altKey) {
                    deltaX = -i.containerWidth;
                } else {
                    deltaX = -30;
                }
                break;
            case 38: // up
                if (e.metaKey) {
                    deltaY = i.contentHeight;
                } else if (e.altKey) {
                    deltaY = i.containerHeight;
                } else {
                    deltaY = 30;
                }
                break;
            case 39: // right
                if (e.metaKey) {
                    deltaX = i.contentWidth;
                } else if (e.altKey) {
                    deltaX = i.containerWidth;
                } else {
                    deltaX = 30;
                }
                break;
            case 40: // down
                if (e.metaKey) {
                    deltaY = -i.contentHeight;
                } else if (e.altKey) {
                    deltaY = -i.containerHeight;
                } else {
                    deltaY = -30;
                }
                break;
            case 32: // space bar
                if (e.shiftKey) {
                    deltaY = i.containerHeight;
                } else {
                    deltaY = -i.containerHeight;
                }
                break;
            case 33: // page up
                deltaY = i.containerHeight;
                break;
            case 34: // page down
                deltaY = -i.containerHeight;
                break;
            case 36: // home
                deltaY = i.contentHeight;
                break;
            case 35: // end
                deltaY = -i.contentHeight;
                break;
            default:
                return;
        }

        if ((i.settings.suppressScrollX && deltaX !== 0) || (i.settings.suppressScrollY && deltaY !== 0))
            return;

        element.scrollTop -= deltaY;
        element.scrollLeft += deltaX;
        updateGeometry(i);

        if (shouldPreventDefault(deltaX, deltaY))
            e.preventDefault();
    });
};

let wheel = function (i) {
    let element = i.element;

    function shouldPreventDefault(deltaX, deltaY) {
        let roundedScrollTop = Math.floor(element.scrollTop);
        let isTop = element.scrollTop === 0;
        let isBottom =
            roundedScrollTop + element.offsetHeight === element.scrollHeight;
        let isLeft = element.scrollLeft === 0;
        let isRight =
            element.scrollLeft + element.offsetWidth === element.scrollWidth;

        let hitsBound;

        // pick axis with primary direction
        if (Math.abs(deltaY) > Math.abs(deltaX)) {
            hitsBound = isTop || isBottom;
        } else {
            hitsBound = isLeft || isRight;
        }

        return hitsBound ? !i.settings.wheelPropagation : true;
    }

    function getDeltaFromEvent(e) {
        let deltaX = e.deltaX;
        let deltaY = -1 * e.deltaY;

        if (typeof deltaX === 'undefined' || typeof deltaY === 'undefined') {
            // OS X Safari
            deltaX = -1 * e.wheelDeltaX / 6;
            deltaY = e.wheelDeltaY / 6;
        }

        if (e.deltaMode && e.deltaMode === 1) {
            // Firefox in deltaMode 1: Line scrolling
            deltaX *= 10;
            deltaY *= 10;
        }

        if (deltaX !== deltaX && deltaY !== deltaY /* NaN checks */) {
            // IE in some mouse drivers
            deltaX = 0;
            deltaY = e.wheelDelta;
        }

        // Reverse axis with shift key should be executed only on windows
        if (window.navigator.userAgent.indexOf('Windows') > 0) {
            if (e.shiftKey) // reverse axis with shift key
                return [-deltaY, -deltaX];
        }
        return [deltaX, deltaY];
    }

    function shouldBeConsumedByChild(target, deltaX, deltaY) {
        // FIXME: this is a workaround for <select> issue in FF and IE #571
        if (!env.isWebKit && element.querySelector('select:focus')) {
            return true;
        }

        if (!element.contains(target)) {
            return false;
        }

        let cursor = target;

        while (cursor && cursor !== element) {
            if (cursor.classList.contains(cls.element.consuming))
                return true;

            const style = get(cursor);

            // if deltaY && vertical scrollable
            if (deltaY && style.overflowY.match(/(scroll|auto)/)) {
                const maxScrollTop = cursor.scrollHeight - cursor.clientHeight;

                if (maxScrollTop > 0) {
                    if ((cursor.scrollTop > 0 && deltaY < 0) || (cursor.scrollTop < maxScrollTop && deltaY > 0))
                        return true;
                }

            }

            // if deltaX && horizontal scrollable
            if (deltaX && style.overflowX.match(/(scroll|auto)/)) {
                const maxScrollLeft = cursor.scrollWidth - cursor.clientWidth;

                if (maxScrollLeft > 0) {
                    if ((cursor.scrollLeft > 0 && deltaX < 0) || (cursor.scrollLeft < maxScrollLeft && deltaX > 0))
                        return true;
                }
            }

            cursor = cursor.parentNode;
        }

        return false;
    }

    function mousewheelHandler(e) {
        if (i.isFrozen)
            return;

        let ref = getDeltaFromEvent(e);
        let deltaX = ref[0];
        let deltaY = ref[1];

        if (shouldBeConsumedByChild(e.target, deltaX, deltaY)) {
            return;
        }

        let shouldPrevent = false;
        if (!i.settings.useBothWheelAxes) {
            // deltaX will only be used for horizontal scrolling and deltaY will
            // only be used for vertical scrolling - this is the default
            element.scrollTop -= deltaY * i.settings.wheelSpeed;
            element.scrollLeft += deltaX * i.settings.wheelSpeed;
        } else if (i.scrollbarYActive && !i.scrollbarXActive) {
            // only vertical scrollbar is active and useBothWheelAxes option is
            // active, so let's scroll vertical bar using both mouse wheel axes
            if (deltaY) {
                element.scrollTop -= deltaY * i.settings.wheelSpeed;
            } else {
                element.scrollTop += deltaX * i.settings.wheelSpeed;
            }
            shouldPrevent = true;
        } else if (i.scrollbarXActive && !i.scrollbarYActive) {
            // useBothWheelAxes and only horizontal bar is active, so use both
            // wheel axes for horizontal bar
            if (deltaX) {
                element.scrollLeft += deltaX * i.settings.wheelSpeed;
            } else {
                element.scrollLeft -= deltaY * i.settings.wheelSpeed;
            }
            shouldPrevent = true;
        }

        updateGeometry(i);

        shouldPrevent = shouldPrevent || shouldPreventDefault(deltaX, deltaY);
        if (shouldPrevent && !e.ctrlKey) {
            e.stopPropagation();
            e.preventDefault();
        }
    }

    if (typeof window.onwheel !== 'undefined') {
        i.event.bind(element, 'wheel', mousewheelHandler);
    } else if (typeof window.onmousewheel !== 'undefined') {
        i.event.bind(element, 'mousewheel', mousewheelHandler);
    }
};

let touch = function (i) {
    if (!env.supportsTouch && !env.supportsIePointer)
        return;

    let element = i.element;

    function shouldPrevent(deltaX, deltaY) {
        let scrollTop = Math.floor(element.scrollTop);
        let scrollLeft = element.scrollLeft;
        let magnitudeX = Math.abs(deltaX);
        let magnitudeY = Math.abs(deltaY);

        if (magnitudeY > magnitudeX) {
            // user is perhaps trying to swipe up/down the page

            if (
                (deltaY < 0 && scrollTop === i.contentHeight - i.containerHeight) ||
                (deltaY > 0 && scrollTop === 0)
            ) {
                // set prevent for mobile Chrome refresh
                return window.scrollY === 0 && deltaY > 0 && env.isChrome;
            }
        } else if (magnitudeX > magnitudeY) {
            // user is perhaps trying to swipe left/right across the page

            if (
                (deltaX < 0 && scrollLeft === i.contentWidth - i.containerWidth) ||
                (deltaX > 0 && scrollLeft === 0)
            ) {
                return true;
            }
        }

        return true;
    }

    function applyTouchMove(differenceX, differenceY) {
        if (i.isFrozen)
            return;

        element.scrollTop -= differenceY;
        element.scrollLeft -= differenceX;

        updateGeometry(i);
    }

    let startOffset = {};
    let startTime = 0;
    let speed = {};
    let easingLoop = null;

    function getTouch(e) {
        if (e.targetTouches)
            return e.targetTouches[0];
        else
            return e; // Maybe IE pointer
    }

    function shouldHandle(e) {
        if (e.pointerType && e.pointerType === 'pen' && e.buttons === 0)
            return false;
        else if (e.targetTouches && e.targetTouches.length === 1)
            return true;
        else if (e.pointerType && e.pointerType !== 'mouse' && e.pointerType !== e.MSPOINTER_TYPE_MOUSE)
            return true;

        return false;
    }

    function touchStart(e) {
        if (!shouldHandle(e))
            return;

        const touch = getTouch(e);

        startOffset.pageX = touch.pageX;
        startOffset.pageY = touch.pageY;

        startTime = new Date().getTime();

        if (easingLoop !== null)
            clearInterval(easingLoop);
    }

    function shouldBeConsumedByChild(target, deltaX, deltaY) {
        if (!element.contains(target))
            return false;

        let cursor = target;

        while (cursor && cursor !== element) {
            if (cursor.classList.contains(cls.element.consuming))
                return true;

            let style = get(cursor);

            // if deltaY && vertical scrollable
            if (deltaY && style.overflowY.match(/(scroll|auto)/)) {
                const maxScrollTop = cursor.scrollHeight - cursor.clientHeight;

                if (maxScrollTop > 0) {
                    if ((cursor.scrollTop > 0 && deltaY < 0) || (cursor.scrollTop < maxScrollTop && deltaY > 0))
                        return true;
                }
            }

            // if deltaX && horizontal scrollable
            if (deltaX && style.overflowX.match(/(scroll|auto)/)) {
                const maxScrollLeft = cursor.scrollWidth - cursor.clientWidth;

                if (maxScrollLeft > 0) {
                    if ((cursor.scrollLeft > 0 && deltaX < 0) || (cursor.scrollLeft < maxScrollLeft && deltaX > 0))
                        return true;
                }
            }

            cursor = cursor.parentNode;
        }

        return false;
    }

    function touchMove(e) {
        if (shouldHandle(e)) {
            let touch = getTouch(e);

            let currentOffset = { pageX: touch.pageX, pageY: touch.pageY };

            let differenceX = currentOffset.pageX - startOffset.pageX;
            let differenceY = currentOffset.pageY - startOffset.pageY;

            if (shouldBeConsumedByChild(e.target, differenceX, differenceY)) {
                return;
            }

            applyTouchMove(differenceX, differenceY);
            startOffset = currentOffset;

            let currentTime = new Date().getTime();

            let timeGap = currentTime - startTime;
            if (timeGap > 0) {
                speed.x = differenceX / timeGap;
                speed.y = differenceY / timeGap;
                startTime = currentTime;
            }

            if (e.cancelable && shouldPrevent(differenceX, differenceY)) {
                e.preventDefault();
            }
        }
    }
    function touchEnd() {
        if (i.settings.swipeEasing) {
            clearInterval(easingLoop);
            easingLoop = setInterval(function () {
                if (i.isAlive) {
                    clearInterval(easingLoop);
                    return;
                }

                if (!speed.x && !speed.y) {
                    clearInterval(easingLoop);
                    return;
                }

                if (Math.abs(speed.x) < 0.01 && Math.abs(speed.y) < 0.01) {
                    clearInterval(easingLoop);
                    return;
                }

                applyTouchMove(speed.x * 30, speed.y * 30);

                speed.x *= 0.8;
                speed.y *= 0.8;
            }, 10);
        }
    }

    if (env.supportsTouch) {
        i.event.bind(element, 'touchstart', touchStart);
        i.event.bind(element, 'touchmove', touchMove);
        i.event.bind(element, 'touchend', touchEnd);
    } else if (env.supportsIePointer) {
        if (window.PointerEvent) {
            i.event.bind(element, 'pointerdown', touchStart);
            i.event.bind(element, 'pointermove', touchMove);
            i.event.bind(element, 'pointerup', touchEnd);
        } else if (window.MSPointerEvent) {
            i.event.bind(element, 'MSPointerDown', touchStart);
            i.event.bind(element, 'MSPointerMove', touchMove);
            i.event.bind(element, 'MSPointerUp', touchEnd);
        }
    }
};

let defaultSettings = function () {
    return ({
        handlers: ['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch'],
        maxScrollbarLength: null,
        minScrollbarLength: null,
        scrollingThreshold: 1000,
        scrollXMarginOffset: 0,
        scrollYMarginOffset: 0,
        suppressScrollX: false,
        suppressScrollY: false,
        swipeEasing: true,
        useBothWheelAxes: false,
        wheelPropagation: true,
        wheelSpeed: 1,
    });
};

let handlers = {
    'click-rail': clickRail,
    'drag-thumb': dragThumb,
    keyboard: keyboard,
    wheel: wheel,
    touch: touch,
};

let PerfectScrollbar = function PerfectScrollbar(element, userSettings) {
    const this$1 = this;
    if (userSettings === void 0) userSettings = {};

    if (typeof element === 'string')
        element = document.querySelector(element);

    if (!element || !element.nodeName)
        throw new Error('no element is specified to initialize PerfectScrollbar');

    this.element = element;

    element.classList.add(cls.main);

    this.settings = defaultSettings();
    for (let key in userSettings) {
        this$1.settings[key] = userSettings[key];
    }

    this.containerWidth = null;
    this.containerHeight = null;
    this.contentWidth = null;
    this.contentHeight = null;

    let focus = function () { return element.classList.add(cls.state.focus); };
    let blur = function () { return element.classList.remove(cls.state.focus); };

    this.isRtl = get(element).direction === 'rtl';
    this.isNegativeScroll = (function () {
        let originalScrollLeft = element.scrollLeft;
        let result = null;
        element.scrollLeft = -1;
        result = element.scrollLeft < 0;
        element.scrollLeft = originalScrollLeft;
        return result;
    })();
    this.negativeScrollAdjustment = this.isNegativeScroll
        ? element.scrollWidth - element.clientWidth
        : 0;
    this.event = new EventManager();
    this.ownerDocument = element.ownerDocument || document;

    this.scrollbarXRail = div(cls.element.rail('x'));
    element.appendChild(this.scrollbarXRail);
    this.scrollbarX = div(cls.element.thumb('x'));
    this.scrollbarXRail.appendChild(this.scrollbarX);
    this.scrollbarX.setAttribute('tabindex', 0);
    this.event.bind(this.scrollbarX, 'focus', focus);
    this.event.bind(this.scrollbarX, 'blur', blur);
    this.scrollbarXActive = null;
    this.scrollbarXWidth = null;
    this.scrollbarXLeft = null;
    let railXStyle = get(this.scrollbarXRail);
    this.scrollbarXBottom = parseInt(railXStyle.bottom, 10);
    if (isNaN(this.scrollbarXBottom)) {
        this.isScrollbarXUsingBottom = false;
        this.scrollbarXTop = toInt(railXStyle.top);
    } else {
        this.isScrollbarXUsingBottom = true;
    }
    this.railBorderXWidth =
        toInt(railXStyle.borderLeftWidth) + toInt(railXStyle.borderRightWidth);
    // Set rail to display:block to calculate margins
    set(this.scrollbarXRail, { display: 'block' });
    this.railXMarginWidth =
        toInt(railXStyle.marginLeft) + toInt(railXStyle.marginRight);
    set(this.scrollbarXRail, { display: '' });
    this.railXWidth = null;
    this.railXRatio = null;

    this.scrollbarYRail = div(cls.element.rail('y'));
    element.appendChild(this.scrollbarYRail);
    this.scrollbarY = div(cls.element.thumb('y'));
    this.scrollbarYRail.appendChild(this.scrollbarY);
    this.scrollbarY.setAttribute('tabindex', 0);
    this.event.bind(this.scrollbarY, 'focus', focus);
    this.event.bind(this.scrollbarY, 'blur', blur);
    this.scrollbarYActive = null;
    this.scrollbarYHeight = null;
    this.scrollbarYTop = null;
    let railYStyle = get(this.scrollbarYRail);
    this.scrollbarYRight = parseInt(railYStyle.right, 10);
    if (isNaN(this.scrollbarYRight)) {
        this.isScrollbarYUsingRight = false;
        this.scrollbarYLeft = toInt(railYStyle.left);
    } else {
        this.isScrollbarYUsingRight = true;
    }
    this.scrollbarYOuterWidth = this.isRtl ? outerWidth(this.scrollbarY) : null;
    this.railBorderYWidth =
        toInt(railYStyle.borderTopWidth) + toInt(railYStyle.borderBottomWidth);
    set(this.scrollbarYRail, { display: 'block' });
    this.railYMarginHeight =
        toInt(railYStyle.marginTop) + toInt(railYStyle.marginBottom);
    set(this.scrollbarYRail, { display: '' });
    this.railYHeight = null;
    this.railYRatio = null;

    this.reach = {
        x:
            element.scrollLeft <= 0 ? 'start':
                element.scrollLeft >= this.contentWidth - this.containerWidth ? 'end':null,
        y:
            element.scrollTop <= 0
                ? 'start':
                    element.scrollTop >= this.contentHeight - this.containerHeight ? 'end':null,
    };

    this.isAlive = true;
    this.isFrozen = false;

    this.settings.handlers.forEach(function (handlerName) { return handlers[handlerName](this$1); });

    this.lastScrollTop = Math.floor(element.scrollTop); // for onScroll only
    this.lastScrollLeft = element.scrollLeft; // for onScroll only
    this.event.bind(this.element, 'scroll', function (e) { return this$1.onScroll(e); });
    updateGeometry(this);
};

PerfectScrollbar.prototype.update = function update() {
    if (!this.isAlive) {
        return;
    }

    // Recalcuate negative scrollLeft adjustment
    this.negativeScrollAdjustment = this.isNegativeScroll
        ? this.element.scrollWidth - this.element.clientWidth
        : 0;

    // Recalculate rail margins
    set(this.scrollbarXRail, { display: 'block' });
    set(this.scrollbarYRail, { display: 'block' });
    this.railXMarginWidth =
        toInt(get(this.scrollbarXRail).marginLeft) +
        toInt(get(this.scrollbarXRail).marginRight);
    this.railYMarginHeight =
        toInt(get(this.scrollbarYRail).marginTop) +
        toInt(get(this.scrollbarYRail).marginBottom);

    // Hide scrollbars not to affect scrollWidth and scrollHeight
    set(this.scrollbarXRail, { display: 'none' });
    set(this.scrollbarYRail, { display: 'none' });

    updateGeometry(this);

    processScrollDiff(this, 'top', 0, false, true);
    processScrollDiff(this, 'left', 0, false, true);

    set(this.scrollbarXRail, { display: '' });
    set(this.scrollbarYRail, { display: '' });
};

PerfectScrollbar.prototype.onScroll = function onScroll(e) {
    if (!this.isAlive || this.isFrozen)
        return;

    updateGeometry(this);
    processScrollDiff(this, 'top', this.element.scrollTop - this.lastScrollTop);
    processScrollDiff(this, 'left', this.element.scrollLeft - this.lastScrollLeft);

    this.lastScrollTop = Math.floor(this.element.scrollTop);
    this.lastScrollLeft = this.element.scrollLeft;
};

PerfectScrollbar.prototype.freeze = function freeze() {
    this.isFrozen = true;

    this.resetPosition();
}

PerfectScrollbar.prototype.unfreeze = function unfreeze() {
    this.isFrozen = false;
}

PerfectScrollbar.prototype.resetPosition = function resetPosition() {
    this.element.scrollTop = 0;
    this.element.scrollLeft = 0;

    updateGeometry(this);
}

PerfectScrollbar.prototype.destroy = function destroy() {
    if (!this.isAlive)
        return;

    this.event.unbindAll();
    remove(this.scrollbarX);
    remove(this.scrollbarY);
    remove(this.scrollbarXRail);
    remove(this.scrollbarYRail);
    this.removePsClasses();

    // unset elements
    this.element = null;
    this.scrollbarX = null;
    this.scrollbarY = null;
    this.scrollbarXRail = null;
    this.scrollbarYRail = null;
    this.isAlive = false;
    this.isFrozen = false;
};

PerfectScrollbar.prototype.removePsClasses = function removePsClasses() {
    this.element.className = this.element.className
        .split(' ')
        .filter(function (name) { return !name.match(/^ps([-_].+|)$/); })
        .join(' ');
};

module.exports = PerfectScrollbar;
